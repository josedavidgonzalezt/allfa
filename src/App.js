import React,{Fragment} from 'react';
import { BrowserRouter as Router,Switch, Route } from 'react-router-dom';
import Inicio from './components/layout/inicio/Inicio';
import Mapa from './components/layout/mapa/Mapa';
import MapaState from './context/mapaState';


function App() {
  return (
  
  <Fragment>
    <MapaState>
      <Router>
        <Switch>
          <Route exact path="/inicio" component={Inicio}/>
          <Route exact path="/mapa" component={Mapa}/>
        </Switch>
      </Router>
    </MapaState>
    

  </Fragment>
  );
}

export default App;
