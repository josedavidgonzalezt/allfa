import { OCULTAR_ESCUELAS,
    MOSTRAR_ESCUELAS,
    OCULTAR_CLUBES,
    MOSTRAR_CLUBES,
    OCULTAR_POLVORINES,
    MOSTRAR_POLVORINES,
    OCULTAR_VMAYO,
    MOSTRAR_VMAYO,
    OCULTAR_SOURDEAUX,
    MOSTRAR_SOURDEAUX,
    OCULTAR_NOGUES,
    MOSTRAR_NOGUES,
    OCULTAR_TRIANGULO,
    MOSTRAR_TRIANGULO,
    OCULTAR_GRANDBOURG,
    MOSTRAR_GRANDBOURG,
    OCULTAR_TORTUGUITAS,
    MOSTRAR_TORTUGUITAS,
    OCULTAR_TIERRASALTAS,
    MOSTRAR_TIERRASALTAS
     } from '../types';


export default (state,action)=>{
    switch (action.type) {

        case OCULTAR_ESCUELAS:
            return {
                ...state,
                escuelas: false
            }
            
        case MOSTRAR_ESCUELAS:
        return {
            ...state,
            escuelas:true
        }

        case OCULTAR_CLUBES:
            return {
                ...state,
                clubes:false
            }

        case MOSTRAR_CLUBES:
            return{
                ...state,
                clubes:true
            }

            //-----------------------------------locadlidades--------------------------//
        case OCULTAR_POLVORINES:
            return{
                ...state,
                polvorines: state.localidades.polvorines=false
            }
        case MOSTRAR_POLVORINES:
            return{
                ...state,
                polvorines: state.localidades.polvorines=true
            }


        case OCULTAR_VMAYO:
            return{
                ...state,
                vmayo: state.localidades.vmayo=false
            }
        case MOSTRAR_VMAYO:
            return{
                ...state,
                vmayo: state.localidades.vmayo=true
            }


        case OCULTAR_SOURDEAUX:
            return{
                ...state,
                sourdeaux: state.localidades.sourdeaux=false
            }
        case MOSTRAR_SOURDEAUX:
            return{
                ...state,
                sourdeaux: state.localidades.sourdeaux=true
            }


        case OCULTAR_NOGUES:
            return{
                ...state,
                nogues: state.localidades.nogues=false
            }
        case MOSTRAR_NOGUES:
            return{
                ...state,
                nogues: state.localidades.nogues=true
            }

        case OCULTAR_TRIANGULO:
            return{
                    ...state,
                    triangulo: state.localidades.triangulo=false
            }
         case MOSTRAR_TRIANGULO:
            return{
                    ...state,
                    triangulo: state.localidades.triangulo=true
            }

        case OCULTAR_GRANDBOURG:
             return{
                    ...state,
                    grandbourg: state.localidades.grandbourg=false
                }

        case MOSTRAR_GRANDBOURG:
            return{
                    ...state,
                    grandbourg: state.localidades.grandbourg=true
                }

        case OCULTAR_TORTUGUITAS:
            return{
                    ...state,
                    tortuguitas: state.localidades.tortuguitas=false
                       }
       
        case MOSTRAR_TORTUGUITAS:
                return{
                    ...state,
                    tortuguitas: state.localidades.tortuguitas=true
                       }

        case OCULTAR_TIERRASALTAS:
            return{
                    ...state,
                    tierrasaltas: state.localidades.tierrasaltas=false
                       }
       
        case MOSTRAR_TIERRASALTAS:
                return{
                    ...state,
                    tierrasaltas: state.localidades.tierrasaltas=true
                       }
        default:
            return state;
    }
}