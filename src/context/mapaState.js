import React,{useReducer} from 'react';

import mapaContext from './mapaContext';
import mapaReducer from './mapaReducer';
import { OCULTAR_ESCUELAS,
        MOSTRAR_ESCUELAS,
        OCULTAR_CLUBES,
        MOSTRAR_CLUBES,
        OCULTAR_POLVORINES,
        MOSTRAR_POLVORINES,
        OCULTAR_VMAYO,
        MOSTRAR_VMAYO,
        OCULTAR_SOURDEAUX,
        MOSTRAR_SOURDEAUX,
        OCULTAR_NOGUES,
        MOSTRAR_NOGUES,
        OCULTAR_TRIANGULO,
        MOSTRAR_TRIANGULO,
        OCULTAR_GRANDBOURG,
        MOSTRAR_GRANDBOURG,
        OCULTAR_TORTUGUITAS,
        MOSTRAR_TORTUGUITAS,
        OCULTAR_TIERRASALTAS,
        MOSTRAR_TIERRASALTAS
         } from '../types';

const MapaState = props => {

    const initialState={
        escuelas: false,
        clubes:false,
        localidades:{
                    polvorines:false,
                    vmayo:false,
                    sourdeaux:false,
                    nogues:false,
                    triangulo:false,
                    grandbourg:false,
                    tortuguitas:false,
                    tierrasaltas:false    
                }
    }

    // dispatch para ejecutar las acciones
    const [state, dispatch] = useReducer(mapaReducer,initialState);
    
    //oculta escuelas
    const ocultarEscuela =()=>{
        dispatch({
            type: OCULTAR_ESCUELAS
        })
    }
    //mostrar escuelas
    const mostrarEscuelas =()=>{

        dispatch({
            type: MOSTRAR_ESCUELAS
        })
    }

    //ocultar clubes
    const ocultarClubes=()=>{
        dispatch({
            type:OCULTAR_CLUBES
        })
    }

    //mostrar clubes 
    const mostrarClubes =()=>{
        dispatch({
            type:MOSTRAR_CLUBES
        })
    }

    // ocultar/mostrar Delimitaciones
            //Delimitacion polvorines
    const ocultarPolvorines =()=>{
        dispatch({
            type:OCULTAR_POLVORINES
        })
    }

    const mostrarPolvorines =()=>{
        dispatch({
            type:MOSTRAR_POLVORINES
        })
    }

    //Delimitacion Villa de mayo
    const ocultarVMayo =()=>{
        dispatch({
            type:OCULTAR_VMAYO
        })
    }

    const mostrarVMayo =()=>{
        dispatch({
            type:MOSTRAR_VMAYO
        })
    }

    //Delimitacion Sourdeaux
    const ocultarSourdeaux =()=>{
        dispatch({
            type:OCULTAR_SOURDEAUX
        })
    }

    const mostrarSourdeaux =()=>{
        dispatch({
            type:MOSTRAR_SOURDEAUX
        })
    }


    //Delimitacion Nogues
    const ocultarNogues =()=>{
        dispatch({
            type:OCULTAR_NOGUES
        })
    }

    const mostrarNogues =()=>{
        dispatch({
            type:MOSTRAR_NOGUES
        })
    }

    //Delimitacion triangulo
    const ocultarTriangulo =()=>{
        dispatch({
            type:OCULTAR_TRIANGULO
        })
    }

    const mostrarTriangulo =()=>{
        dispatch({
            type:MOSTRAR_TRIANGULO
        })
    }

    //Delimitacion Grand Bourg
    const ocultarGrandBourg =()=>{
        dispatch({
            type:OCULTAR_GRANDBOURG
        })
    }

    const mostrarGrandBourg =()=>{
        dispatch({
            type:MOSTRAR_GRANDBOURG
        })
    }

    //Delimitacion Tortuguitas
    const ocultarTortuguitas =()=>{
        dispatch({
            type:OCULTAR_TORTUGUITAS
        })
    }

    const mostrarTortuguitas =()=>{
        dispatch({
            type:MOSTRAR_TORTUGUITAS
        })
    }

    //Delimitacion Tierras Altas
    const ocultarTierrasAltas =()=>{
        dispatch({
            type:OCULTAR_TIERRASALTAS
        })
    }

    const mostrarTierrasAltas =()=>{
        dispatch({
            type:MOSTRAR_TIERRASALTAS
        })
    }



    
 
 
    return ( 
        <mapaContext.Provider
            value={{
                escuelas: state.escuelas,
                clubes: state.clubes,
                polvorines: state.localidades.polvorines,
                vmayo: state.localidades.vmayo,
                sourdeaux: state.localidades.sourdeaux,
                nogues: state.localidades.nogues,
                triangulo: state.localidades.triangulo,
                grandbourg: state.localidades.grandbourg,
                tortuguitas: state.localidades.tortuguitas,
                tierrasaltas: state.localidades.tierrasaltas,
                
                ocultarEscuela,
                mostrarEscuelas,
                ocultarClubes,
                mostrarClubes,
                ocultarPolvorines,
                mostrarPolvorines,
                ocultarVMayo,
                mostrarVMayo,
                ocultarSourdeaux,
                mostrarSourdeaux,
                ocultarNogues,
                mostrarNogues,
                ocultarTriangulo,
                mostrarTriangulo,
                ocultarGrandBourg,
                mostrarGrandBourg,
                ocultarTortuguitas,
                mostrarTortuguitas,
                ocultarTierrasAltas,
                mostrarTierrasAltas
                
                
            }}
        >
            {props.children}
        </mapaContext.Provider>
     );
}
 
export default MapaState;