import React,{Fragment, useState} from 'react';
import {Marker, Popup} from 'react-leaflet'
import * as clubes from '../../../data/clubs_malvinas.json';
import {icoClub} from './iconos/iconos';

const Clubes = () => {

    const [puntoclubactivo, setPuntoClubActivo] = useState(null);
    return ( 
        <Fragment>
            {clubes.features.map(club=>(
                <Marker
                    icon={icoClub}
                    key={club.properties.Field1[0]}
                    position={[club.properties.Field1[0],
                                club.properties.Field1[1]]}
                     onClick={()=>setPuntoClubActivo(club)}
                />
            ))}

           {puntoclubactivo &&
            
            <Popup
                position={[
                            puntoclubactivo.properties.Field1[0],
                            puntoclubactivo.properties.Field1[1]
                        ]} 
                onClose={()=>setPuntoClubActivo(null)}
            >

                    <div className="mypopup">
                        <h5>Nombre: {puntoclubactivo.properties.Field2}</h5>
                        <h5>Dirección: {puntoclubactivo.properties.Field3}</h5>
                        <h5>Localidad: {puntoclubactivo.properties.Field4}</h5>
                    </div>
            </Popup>
            }

        </Fragment>
        
     );
}
 
export default Clubes;