import {Icon} from 'leaflet';


export const icoEscuela = new Icon({
    iconUrl: 'https://image.flaticon.com/icons/svg/3225/3225759.svg',
    iconSize: [30, 30]
  })

  export const icoClub = new Icon({
    iconUrl: 'https://image.flaticon.com/icons/svg/787/787535.svg',
    iconSize: [30, 30]
  })

