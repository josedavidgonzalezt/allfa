import React, {Fragment,useContext} from 'react';
import {Map,TileLayer,Polyline, LayersControl} from 'react-leaflet';
import Escuelas from './Escuelas';
import Seleccione from './Seleccione';

import mapaContext from '../../../context/mapaContext';
import Clubes from './Clubes';
import Localidades from './Localidades';

const Mapa = () => {
    const mapasContext = useContext(mapaContext);
    const {escuelas,clubes} =mapasContext ;

    return ( 
        <Fragment>
            <Map center={[-34.4868844,-58.6918288]} zoom={13}  >
            <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
            <LayersControl position="topleft">
            <LayersControl.BaseLayer name="Mapa">
                    <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                </LayersControl.BaseLayer>
                <LayersControl.BaseLayer name="Modo Oscuro">
                    <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png"
                    />
                </LayersControl.BaseLayer>
                <LayersControl.BaseLayer name="Satelite">
                    <TileLayer
                    attribution='Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
                    url="https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}"
                    />
                </LayersControl.BaseLayer>
                
            </LayersControl>
                
                
                <Seleccione/>
                {escuelas ?<Escuelas/> :null}
                {clubes ?<Clubes/> :null}

            
                <Polyline
                    positions={[[-34.532791,-58.7020072],[-34.525736,-58.6941612],[-34.5300994,-58.6886563],[-34.5013833,-58.6466707],[-34.4746599,-58.6781937],[-34.4398133,-58.7061273],[-34.4596286,-58.7437743],[-34.4635484,-58.7444612],[-34.4676838,-58.7521831],[-34.4624449,-58.7564392],[-34.4664733,-58.7640529],[-34.4667756,-58.7645482],[-34.4672152,-58.7648531],[-34.4647343,-58.7702348],[-34.4659762,-58.7696884],[-34.4683231,-58.771323],[-34.4723183,-58.7787225],[-34.472096,-58.778929],[-34.4757606,-58.7856429],[-34.4890542,-58.7711891],[-34.4951419,-58.7609328],[-34.5000679,-58.7498973],[-34.5082921,-58.7293791],[-34.5163307,-58.7231981],[-34.5202254,-58.7187879],[-34.532791,-58.7020072]]}
                    color="blue"
                />

                <Localidades/>
            </Map>
        </Fragment>
     );
}
 
export default Mapa;