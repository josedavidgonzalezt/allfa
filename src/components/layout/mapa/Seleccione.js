import React, {useContext,Fragment, useState} from 'react';
import mapaContext from '../../../context/mapaContext';
import arrowleft from './iconos/bx-left-arrow.svg'
import arrowright from './iconos/bx-right-arrow.svg'
import Voluntarios from './Voluntarios';
import Barrios from './Barrios';


const Seleccione = () => {
    const mapasContext = useContext(mapaContext);
    const {escuelas, ocultarEscuela,mostrarEscuelas,ocultarClubes,clubes,mostrarClubes,polvorines,ocultarPolvorines,mostrarPolvorines, vmayo, ocultarVMayo,mostrarVMayo, sourdeaux, ocultarSourdeaux,mostrarSourdeaux, nogues,ocultarNogues,mostrarNogues,triangulo,ocultarTriangulo,mostrarTriangulo,grandbourg,ocultarGrandBourg,mostrarGrandBourg,tortuguitas,ocultarTortuguitas, mostrarTortuguitas,tierrasaltas,mostrarTierrasAltas,ocultarTierrasAltas} =mapasContext ;

    const [mostrar, guardarMostrar]=useState(false);
    let clasemostar;

    const mostrarMenu=()=>{
        guardarMostrar(true);
    }
    const ocultarMenu=()=>{
        guardarMostrar(false);
    }
    const mostrarClase=()=>{
        if (mostrar===true){
            clasemostar='mostrar';
            return clasemostar
        }

        return null;
    }
    mostrarClase();

  /*  
  codigo para revirsar los checked, colocando en un input defaultChecked={estado} onClick={()=>ocultarMostrar()}
  
  const [tortuguitas, guardarTortuguitas]=useState({tortuga:true});
    
    

    const ocultarMostrar =()=>{
        if(!tortuguitas.tortuga){
            guardarTortuguitas(true);
        }
        
        else if(tortuguitas.tortuga){
            guardarTortuguitas(false);
        }
        
    } */

    return ( 

        <Fragment>

        <div className={`contedor-menu ${clasemostar}`}>
            {mostrar ?<img className="icon-meu" src={arrowright} alt="icono flecha" onClick={ocultarMenu}/>
            :<img className="icon-meu" src={arrowleft} alt="icono flecha" onClick={mostrarMenu}/>}
            
            

        
            <div className="seleccion">
                <h3>Puntos de Interés</h3>
                <label>Escuelas {escuelas ?<input className="escuelas" type="checkbox" defaultChecked onClick={ocultarEscuela}/>
                                        :<input className="escuelas" type="checkbox" onClick={mostrarEscuelas} />}</label>

                <label>Clubes {clubes ?<input className="escuelas" type="checkbox" defaultChecked onClick={ocultarClubes}/>
                                    :<input className="escuelas" type="checkbox" onClick={mostrarClubes} />}</label>
            </div>

            <div className="localidades">
                <h3>Localidades</h3>
                <label style={{color:'#d9d909'}}>Sourdeaux {sourdeaux ? <input type="checkbox" defaultChecked onClick={ocultarSourdeaux}/>
                                                  : <input type="checkbox" onClick={mostrarSourdeaux}/>}</label>

                <label style={{color:'#F7070C'}}>Villa de Mayo {vmayo ? <input type="checkbox" defaultChecked onClick={ocultarVMayo}/>
                                                  : <input type="checkbox" onClick={mostrarVMayo}/>}</label>

                <label style={{color:'#3388FF'}}>Los Polvorines {polvorines ? <input type="checkbox" defaultChecked onClick={ocultarPolvorines}/>
                                                  : <input type="checkbox" onClick={mostrarPolvorines}/>}</label>

                <label style={{color:'#048106'}}>Nogues {nogues ? <input type="checkbox" defaultChecked onClick={ocultarNogues}/>
                                                  : <input type="checkbox" onClick={mostrarNogues}/>}</label>

                <label style={{color:'#ff6205'}}>Grand Bourg {grandbourg ? <input type="checkbox" defaultChecked={true} onClick={ocultarGrandBourg}/>
                                                  : <input type="checkbox" onClick={mostrarGrandBourg}/>}</label>

                <label style={{color:'#000'}}>Tierras Altas {tierrasaltas ? <input type="checkbox" defaultChecked onClick={ocultarTierrasAltas}/>
                                                  : <input type="checkbox" onClick={mostrarTierrasAltas}/>}</label> 

                <label style={{color:'#6309d9'}}>Tortuguitas {tortuguitas ? <input type="checkbox" defaultChecked onClick={ocultarTortuguitas}/>
                                                  : <input type="checkbox" onClick={mostrarTortuguitas}/>}</label>

                <label style={{color:'#cd3ffc'}}>El triangulo {triangulo ? <input type="checkbox" defaultChecked onClick={ocultarTriangulo}/>
                                                  : <input type="checkbox" onClick={mostrarTriangulo}/>}</label>
                                                  
            </div>
            <Voluntarios/>
            <Barrios/>
        </div>
        </Fragment>
     );
}
 
export default Seleccione;