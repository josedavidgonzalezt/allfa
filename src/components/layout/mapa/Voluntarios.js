import React, { useState, Fragment } from 'react';
import {Circle, Tooltip} from 'react-leaflet';

const Voluntarios = () => {

//state de voluntarios
const [villaDeMayo, guardarVillaDeMayo]=useState(false);
const [sourdeaux, guardarSourdeaux]=useState(false);
const [polvorines, guardarPolvorines]=useState(false);
const [nogues, guardarNogues]=useState(false);
const [grandbourg, guardarGrandBourg]=useState(false);
const [tierrasaltas, guardarTierrasAltas]=useState(false);
const [tortuguita, guardarTortuguita]=useState(false);
const [triangulo, guardarTriangulo]=useState(false);


    const handleCheckVillaDeMayo=()=>{
        if (!villaDeMayo) {guardarVillaDeMayo(true)}
        else if (villaDeMayo) {guardarVillaDeMayo(false) } 
    }

    const handleCheckSordeaux=()=>{
        if (!sourdeaux){ guardarSourdeaux(true)}
        else if (sourdeaux) {guardarSourdeaux(false)}   
    }

    const handlePolvorines=()=>{
        if (!polvorines){ guardarPolvorines(true)}
        else if (polvorines) {guardarPolvorines(false)}   
    }

    const handleNogues=()=>{
        if (!nogues){ guardarNogues(true)}
        else if (nogues) {guardarNogues(false)}   
    }
    
    const handleGrandBourg=()=>{
        if (!grandbourg){ guardarGrandBourg(true)}
        else if (grandbourg) {guardarGrandBourg(false)}   
    }
   
    const handleTierrasAltas=()=>{
        if (!tierrasaltas){ guardarTierrasAltas(true)}
        else if (tierrasaltas) {guardarTierrasAltas(false)}   
    }

    const handleTortuguita=()=>{
        if (!tortuguita){ guardarTortuguita(true)}
        else if (tortuguita) {guardarTortuguita(false)}   
    }
   
    const handleTriangulo=()=>{
        if (!triangulo){ guardarTriangulo(true)}
        else if (triangulo) {guardarTriangulo(false)}   
    }

    return ( 
        <Fragment>
            <div className="voluntarios">
                <h3>Voluntarios</h3>
                <label>Villa de Mayo <input type="checkbox" defaultChecked={villaDeMayo} onClick={()=>handleCheckVillaDeMayo()} /></label>
                <label>Sourdeaux <input type="checkbox" defaultChecked={sourdeaux} onClick={()=>handleCheckSordeaux()} /></label>
                <label>Polvorines <input type="checkbox" defaultChecked={polvorines} onClick={()=>handlePolvorines()} /></label>
                <label>Nogues <input type="checkbox" defaultChecked={polvorines} onClick={()=>handleNogues()} /></label>
                <label>Grand Bourg <input type="checkbox" defaultChecked={grandbourg} onClick={()=>handleGrandBourg()} /></label>
                <label>Tierras Altas <input type="checkbox" defaultChecked={tierrasaltas} onClick={()=>handleTierrasAltas()} /></label>
                <label>Tortuguitas <input type="checkbox" defaultChecked={tortuguita} onClick={()=>handleTortuguita()} /></label>
                <label>El triangulo <input type="checkbox" defaultChecked={triangulo} onClick={()=>handleTriangulo()} /></label>
            </div>

            {sourdeaux ? <Circle center={[-34.5011768,-58.663293]} radius={700} color="#d9d909">
                           <Tooltip><span>Total Voluntarios: 442</span></Tooltip>
            </Circle> :null}

            {villaDeMayo ? <Circle center={[-34.5118274,-58.6840503]} radius={1500} color="red">
                           <Tooltip><span>Total Voluntarios: 1525</span></Tooltip>
            </Circle> :null}

            {polvorines ? <Circle center={[-34.507507,-58.7096316]} radius={2000} color="#3388ff">
                           <Tooltip><span>Total Voluntarios: 2378</span></Tooltip>
            </Circle> :null}

            {nogues ? <Circle center={[-34.4760881,-58.6979666]} radius={400} color="#048106">
                           <Tooltip><span>Total Voluntarios: 280</span></Tooltip>
            </Circle> :null}

            {grandbourg ? <Circle center={[-34.4864203,-58.7278151]} radius={1000} color="#ff6205">
                           <Tooltip><span>Total Voluntarios: 1080</span></Tooltip>
            </Circle> :null}

            {tierrasaltas ? <Circle center={[-34.4787388,-58.7436902]} radius={100} color="#000">
                           <Tooltip><span>Total Voluntarios: 9</span></Tooltip>
            </Circle> :null}

            {tortuguita ? <Circle center={[-34.4768555,-58.7587978]} radius={1480} color="#6309d9">
                           <Tooltip><span>Total Voluntarios: 1507</span></Tooltip>
            </Circle> :null}
            
            {triangulo ? <Circle center={[-34.4517154,-58.7083219]} radius={300} color="#cd3ffc">
                           <Tooltip><span>Total Voluntarios: 33</span></Tooltip>
            </Circle> :null}

           
            
        
            
    

            
        </Fragment>

     );
}
 
export default Voluntarios;