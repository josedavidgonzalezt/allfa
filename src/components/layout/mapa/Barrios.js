import React,{Fragment,useState} from 'react';
import {Polygon,Tooltip} from 'react-leaflet';
import * as barrios from '../../../data/barrios_malvinas.json';

const Barrios = () => {

    const  [mostrarBarrios,setMostrarBarrios] =useState(false);

    const handleCheckBarrios=()=>{
        if (mostrarBarrios){
            setMostrarBarrios(false);
        }
        else if(!mostrarBarrios){
            setMostrarBarrios(true);
        }
    }

    return ( 
        <Fragment>
            <div className="voluntarios">
                <h3>Barrios <input type="checkbox" defaultChecked={mostrarBarrios} onClick={()=>handleCheckBarrios()}/></h3>
            </div>

            {mostrarBarrios 
                ? barrios.features.map(barrio=>(
                    <Polygon
                    positions={barrio.geometry.coordinates}
                    color="#000"
                ><Tooltip><span>{barrio.properties.nombre}</span></Tooltip></Polygon>

                ))
                :null}
            
        </Fragment>
     );
}
 
export default Barrios;