import React, {Fragment,useState} from 'react';
import {Marker, Popup} from 'react-leaflet';
import * as escuelas from '../../../data/escuelas_malvinas.json';
import {icoEscuela} from './iconos/iconos';


const Escuelas = () => {

    const [popupedificios, setPopUpEscuelas]= useState(null);
    return ( 
        <Fragment>
            {escuelas.features.map(escuela=>(
                <Marker
                    key={escuela.properties.coordenadas[0]}
                    icon={icoEscuela}
                    position={[
                        escuela.properties.coordenadas[0],
                        escuela.properties.coordenadas[1]
                    ]}
                    onClick={()=>{ setPopUpEscuelas(escuela);}}
                />
               
            ))}
            {popupedificios &&
                <Popup 
                    position={[
                        popupedificios.properties.coordenadas[0],
                        popupedificios.properties.coordenadas[1]
                    ]}
                    onClose={()=>{setPopUpEscuelas(null);}}
                    >
                    
                    <div className="mypopup">
                        <h5>Escuela: {popupedificios.properties.Escuela}</h5>
                        <h5>Dirección: {popupedificios.properties.Direccion}</h5>
                        <h5>Localidad: {popupedificios.properties.Barrio}</h5>
                        <h5>Fiscal: {popupedificios.properties.FiscGral}</h5>
                    </div>
                </Popup>
                
            
            }

        </Fragment>
     );
}
 
export default Escuelas;